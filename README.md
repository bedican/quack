# Quack
Quack is an [Apache Ant](http://ant.apache.org) Task to create self-extracting jar files.

## Installing
Quack can be installed in a number of ways, each gives the result of placing quack.jar on the classpath.

- Place quack.jar in ANT_HOME/lib.
- Add the locaton of quack.jar to the CLASSPATH environment variable.
- Specify the location of quack.jar using a <classpath> element in a <taskdef> in the buildfile.
- Specify the location of quack.jar using the classpath attribute of <taskdef> in the buildfile.

## Usage
The following demonstrates an example usage of Quack within an Ant buildfile.

```xml
<target name="self-extract">
	<taskdef name="quack" classname="uk.co.bedican.quack.SelfExtractingJarTask" classpath="/path/to/quack.jar" />
	<quack destfile="my-product.jar">
		<fileset dir="my-product" />
	</quack>
</target>
```

## Task Parameters
This task forms an implicit FileSet and supports most attributes of <fileset> (dir becomes basedir) as well as the nested <include>, <exclude> and <patternset> elements.

Or, you may place within it nested file sets. In this case basedir is optional; the implicit file set is only used if basedir is set. You may use any mixture of the implicit file set (with basedir set, and optional attributes like includes and optional subelements like <include>); explicit nested <fileset> elements so long as at least one fileset total is specified.

| Attribute     | Description                                                                                                                             | Required |
|---------------|-----------------------------------------------------------------------------------------------------------------------------------------|----------|
| destfile      | The jar file to create.                                                                                                                 | Yes      |
| basedir       | The directory to pack within the jar.                                                                                                   | No       |
| includes      | Comma or space separated list of patterns of files that must be included. All files are included when omitted.                          | No       |
| excludes      | Comma or space separated list of patterns of files that must be excluded. No files (except default excludes) are excluded when omitted. | No       |
| includesfile  | The name of a file. Each line of this file is taken to be an include pattern.                                                           | No       |
| excludesfile  | The name of a file. Each line of this file is taken to be an exclude pattern.                                                           | No       |

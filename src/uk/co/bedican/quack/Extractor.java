/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.quack;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.zip.*;

public class Extractor
{
	public static final String INSTALL_ROOT = "install/";
	private static final int BUFFER_SIZE = 2048;

	public Extractor()
	{
	}

	public void execute()
	{
		try {
			this.doExtract();
		} catch(IOException ioe) {
			System.out.println(ioe.getMessage());
		}
	}

	private void doExtract() throws IOException
	{
		// Files in a jar are of the format "jar:file:myJar.jar!/path/to/file.class"

		URL classUrl = this.getClass().getClassLoader().getSystemResource(this.getClass().getName().replace(".", "/") + ".class");
		File jarFile = new File(URLDecoder.decode(classUrl.toString().substring(9, classUrl.toString().indexOf("!/")), "UTF-8"));

		ZipFile zipFile = new ZipFile(jarFile);

		BufferedInputStream in = null;
		BufferedOutputStream out = null;

		ZipEntry entry;

		String entryPath;
		File entryFile, extractFile;

		int bytesRead;
		byte[] buffer;

		entry = zipFile.getEntry(INSTALL_ROOT);
		if((entry == null) || (!entry.isDirectory())) {
			throw new IOException("install directory not found");
		}

		File baseExtractDir = new File(jarFile.getName().replace(".jar", "")); // Relative to working directory
		// File baseExtractDir = new File(jarFile.getParentFile(), jarFile.getName().replace(".jar", "")); // relative to jar file
		if((!baseExtractDir.exists()) && (!baseExtractDir.mkdir())) {
			throw new IOException("Failed to create directory " + baseExtractDir.toString());
		}

		Enumeration en = zipFile.entries();

		while(en.hasMoreElements())
		{
			entry = (ZipEntry)en.nextElement();

			if((entry.getName().equals(INSTALL_ROOT)) || (!entry.getName().startsWith(INSTALL_ROOT))) {
				continue;
			}

			entryPath = entry.getName().replaceFirst(INSTALL_ROOT, "");
			entryFile = new File(baseExtractDir.getName(), entryPath);

			extractFile = new File(baseExtractDir, entryPath);

			System.out.println("Extracting: " + extractFile.getPath());

			if(entry.isDirectory())
			{
				if((!extractFile.exists()) && (!extractFile.mkdirs())) {
					throw new IOException("Failed to create directory " + extractFile.getPath());
				}
				continue;
			}

			buffer = new byte[BUFFER_SIZE];

			in = new BufferedInputStream(zipFile.getInputStream(entry));
			out = new BufferedOutputStream(new FileOutputStream(entryFile), BUFFER_SIZE);

			while((bytesRead = in.read(buffer, 0, BUFFER_SIZE)) != -1) {
				out.write(buffer, 0, bytesRead);
			}

			out.flush();
			out.close();
			in.close();
		}
	}

	public static void main(String[] args)
	{
		Extractor extractor = new Extractor();
		extractor.execute();
	}
}
/**
 * (c) Copyright 2010 Bedican Solutions
 * http://www.bedican.co.uk
 */

package uk.co.bedican.quack;

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.jar.*;
import java.util.zip.*;

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;
import org.apache.tools.ant.taskdefs.*;

public class SelfExtractingJarTask extends MatchingTask
{
	private List<FileSet> fileSets;

	private File destFile = null;
	private File baseDir = null;

	public SelfExtractingJarTask()
	{
		this.fileSets = new Vector<FileSet>();
	}

	public void setBasedir(File baseDir)
	{
		this.baseDir = baseDir;
	}

	public void setDestFile(File destFile)
	{
		this.destFile = destFile;
	}

	public void addFileSet(FileSet fileSet)
	{
		this.fileSets.add(fileSet);
	}

	private void buildJar(List<FileSet> fileSets) throws IOException
	{
		// Create new jar file

		java.util.jar.Manifest manifest = new java.util.jar.Manifest();
		Attributes attributes = manifest.getMainAttributes();
		attributes.putValue(Attributes.Name.MANIFEST_VERSION.toString(), "1.0");
		attributes.putValue("Created-By", System.getProperty( "java.version" ) + " (" + System.getProperty( "java.vendor" ) + ")");
		attributes.putValue("Main-Class", "uk.co.bedican.quack.Extractor");

		JarOutputStream jarOutputStream = new JarOutputStream(new BufferedOutputStream(new FileOutputStream(this.destFile)), manifest);

		// Copy Extractor class into the new jar file.

		URL classUrl = SelfExtractingJarTask.class.getResource("/" + this.getClass().getName().replace(".", "/") + ".class");
		File jarFile = new File(URLDecoder.decode(classUrl.toString().substring(10, classUrl.toString().indexOf("!/")), "UTF-8"));

		ZipFile zipFile = new ZipFile(jarFile);
		ZipEntry zipEntry = zipFile.getEntry("uk/co/bedican/quack/Extractor.class");

		jarOutputStream.putNextEntry(zipEntry);

		byte[] buffer = new byte[2048];
		BufferedInputStream jarCopyInputStream = new BufferedInputStream(zipFile.getInputStream(zipEntry));

		int len;

		while((len = jarCopyInputStream.read(buffer)) != -1) {
			jarOutputStream.write(buffer, 0, len);
		}

		jarCopyInputStream.close();

		// Add in the entries from the given filesets.

		FileScanner scanner;
		File baseDir;
		String[] includedFiles;

		File entryFile;
		String entryFilePath;
		String entryDirPath;

		BufferedInputStream jarEntryInputStream;

		List<String> directories = new Vector<String>();
		List<String> putDirectories = new Vector<String>();

		for(FileSet fileSet:fileSets)
		{
			scanner = fileSet.getDirectoryScanner(this.getProject());

			baseDir = scanner.getBasedir();
			includedFiles = scanner.getIncludedFiles();

			for(String includedFile:includedFiles)
			{
				entryFilePath = Extractor.INSTALL_ROOT + includedFile.replace('\\', '/');

				// Add the subdirectories of this entry.

				entryFile = new File(entryFilePath);
				while((entryFile = entryFile.getParentFile()) != null)
				{
					entryDirPath = entryFile.getPath().replace('\\', '/') + "/";
					if(!directories.contains(entryDirPath)) {
						directories.add(0, entryDirPath);
					}
				}
				for(String directory:directories) {
					if(!putDirectories.contains(directory)) {
						putDirectories.add(directory);
						jarOutputStream.putNextEntry(new JarEntry(directory));
					}
				}

				// Add file entry.

				jarOutputStream.putNextEntry(new JarEntry(entryFilePath));

				jarEntryInputStream = new BufferedInputStream(new FileInputStream(new File(baseDir, includedFile)));

				while ((len = jarEntryInputStream.read(buffer)) != -1) {
					jarOutputStream.write(buffer, 0, len);
				}

				jarEntryInputStream.close();
			}
		}

		jarOutputStream.close();
	}

	public void execute() throws BuildException
	{
		if(this.destFile == null) {
			throw new BuildException("destfile attribute has not been specified.");
		}

		List<FileSet> fileSets = new Vector<FileSet>();

		if(this.baseDir != null)
		{
			FileSet implicitFileset = (FileSet)this.getImplicitFileSet().clone();
			implicitFileset.setDir(this.baseDir);
			fileSets.add(implicitFileset);
		}

		for(FileSet fileSet:this.fileSets) {
			fileSets.add(fileSet);
		}

		if(fileSets.isEmpty()) {
			throw new BuildException("No files selected.");
		}

		log("Building jar: " + this.destFile.getAbsolutePath());

		try
		{
			this.buildJar(fileSets);
		}
		catch(IOException ioe)
		{
			throw new BuildException(ioe.getMessage());
		}
	}
}